

class TallyInsignificantItemEndpointIndicator {
  unmeasured = null
  measured = null
  complete = null
  annoyedBy = null
}

class TallyInsignificantTransportationRequestIndicator {
  unrequested = null
  inTransit = null
  arrived = null // respondedTo, allowed, achieved
  blockedBy = null
}

class TallyInsignificantDevelopmentIndicator {
  idea = null
  development = null
  achieved = null
  failedToSatisfyItem = null
}

class TallyInsignificantOrderIndicator {
  unordered = null
  inProcessOfBeingOrdered = null
  ordered = null
  blockedBy = null
}

class TallyInsignificantFunctionEvaluationIndicator {
  createFunction = null
  evaluateFunction = null
  completeFunctionEvaluation = null
  receiveErrorFromFunctionEvaluation = null
}

class TallyInsignificantAnswerToLifeIndicator {
  intention = null
  anticipation = null
  acceptance = null
  acclimation = null
}

class TallyInsignificantAnswerToInterestingIdeasIndicator {
  warning = null
  answer = null
  action = null
  collon = null
}

export {
  TallyInsignificantItemEndpointIndicator,
  TallyInsignificantTransportationRequestIndicator,
  TallyInsignificantDevelopmentIndicator,
  TallyInsignificantOrderIndicator,
  TallyInsignificantFunctionEvaluationIndicator,
  TallyInsignificantAnswerToLifeIndicator,
  TallyInsignificantAnswerToInterestingIdeasIndicator
}

