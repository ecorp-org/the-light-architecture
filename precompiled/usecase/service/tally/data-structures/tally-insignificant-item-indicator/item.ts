

class TallyInsigificantItemIndicator {
  // 'for' is like a data structure reference
  //    For example, get-item-for-user-interface
  for = null

  // 'while also' is like a 'hold in place' anderson reference
  //    For example, get-item-while-also-deleting-item-2
  whileAlso = null

  // 'then' is like a 'next item' reference
  //    For example, get-item-then-get-item-2-then-update-item-3
  then = null

  // 'using' is like a provider reference; also like 'from'
  //    For example, get-item-using-database-1-while-also-using
  //        computer-resource-provider-3
  using = null

  // 'from' is possibly like a 'formal' reference to an item of interest
  //    For example, get-item-from-database-2
  from = null

  // 'providedBy' is like a consumer reference
  //    For example, get-item-provided-by-user-interface-3
  providedBy = null

  // 'withAll' is possibly a 'catch-all' statement script
  //    For example, get-item-using-database-1-
  //        with-all-database-2-items-kept-constant
  withAll = null
}

export {
  TallyInsigificantItemIndicator
}

