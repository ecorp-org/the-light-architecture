
class TallyNameOfInterest {
  gender = null
  period = null
  age = null
  name = null
  rule = null
  service = null
}

class TallyNameOfInterestPrefixMarker {
  gender = ''

  // work-in-progress; not sure if this is the best prefix
  period = 'x'

  // pronounced 'aye'
  age = 'a'

  name = ''

  // pronounced 'dash'
  rule = '-'

  // @ [pronounced 'at'] for folder | /@service/**/*/
  // _ [pronounced 'underscore'] for file | /**/*/_service.ts
  // not always required. Sometimes only for development emphasis
  // like importing and exporting information to and from user
  // as opposed to service code that doesn't need prefix.
  service = ['@', '_']
}

export {
  TallyNameOfInterest,
  TallyNameOfInterestPrefixMarker
}

