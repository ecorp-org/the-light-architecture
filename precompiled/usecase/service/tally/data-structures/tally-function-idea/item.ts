


import { TallyIdeaConstructGeneratorForService } from '../tally-idea-construct-generator/item'

class TallyFunctionIdeaType1 {
  precompiled: TallyFunctionIdeaType2
  compiler: TallyFunctionIdeaType2
  compiled: TallyFunctionIdeaType2
  dialogue: TallyFunctionIdeaType2
}

class TallyFunctionIdeaType2 {
  _service: TallyIdeaConstructGeneratorForService
  usecase: TallyIdeaConstructGeneratorForService
  provider: TallyIdeaConstructGeneratorForService
  consumer: TallyIdeaConstructGeneratorForService
  miscellaneous: TallyIdeaConstructGeneratorForService
}

export {
  TallyFunctionIdeaType1,
  TallyFunctionIdeaType2
}

