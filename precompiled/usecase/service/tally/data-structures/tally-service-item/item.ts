

class TallyServiceItem {
  database = null
  networkProtocol = null
  eventListener = null
  permissionRule = null
  ananaAnanaAnana = null
  scaleMatrixAmana = null
  computerProgramManager = null
}

class TallyServiceIndicatorItem {
  serviceAccount = null
  serviceAdministratorAccount = null
  serviceCarrierCapacityAccount = null
  serviceIgnoranceIndicatorAccount = null
}

export {
  TallyServiceItem,
  TallyServiceIndicatorItem
}
