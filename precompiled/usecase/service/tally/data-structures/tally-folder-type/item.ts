
class TallyFolderType<FolderType> {
  folderName: string = ''
  folderItemList: FolderType[]
}

export {
  TallyFolderType
}
