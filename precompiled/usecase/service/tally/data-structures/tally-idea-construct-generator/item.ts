
import { TallyFolderType } from '../tally-folder-type/item'
import { TallyComputerProgramIndicator } from '../tally-computer-program-indicator/item'

class TallyIdeaConstructGeneratorForService {
  service: TallyFolderType<TallyComputerProgramIndicator>
  serviceGroup: TallyFolderType<TallyComputerProgramIndicator>
  serviceType: TallyFolderType<TallyComputerProgramIndicator>
}

class TallyIdeaConstructGeneratorForDatabase {
  database = null
  databaseStore = null
  databaseStoreItem = null
}

class TallyIdeaConstructGeneratorForEventListener {
  eventListener = null
  eventType = null
  event = null
}

class TallyIdeaConstructGeneratorForNetworkProtocol {
  networkProtocolConnection = null
  networkProtocolMember = null
  networkProtocolMessage = null
}

class TallyIdeaConstructGeneratorForPermissionRule {
  permissionRule = null
  permissionType = null
  permission = null
}

class TallyIdeaConstructGeneratorForInteractiveGraph {
  interactiveGraph = null
  interactiveGraphType = null
  interactive = null
}

class TallyIdeaConstructGeneratorForScaleMatrixAmana {
  scaleMatrixAmana = null
  scaleMatrixType = null
  scaleMatrix = null
}

class TallyIdeaConstructGeneratorForComputerProgramManager {
  computerProgramManager = null
  computerProgramType = null
  computerProgram = null
}

export {
  TallyIdeaConstructGeneratorForService,
  TallyIdeaConstructGeneratorForDatabase,
  TallyIdeaConstructGeneratorForEventListener,
  TallyIdeaConstructGeneratorForNetworkProtocol,
  TallyIdeaConstructGeneratorForPermissionRule,
  TallyIdeaConstructGeneratorForInteractiveGraph,
  TallyIdeaConstructGeneratorForScaleMatrixAmana,
  TallyIdeaConstructGeneratorForComputerProgramManager
}

