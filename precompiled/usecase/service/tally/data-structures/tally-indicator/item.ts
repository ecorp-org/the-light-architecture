

class TallyIndicator {
  basicComputerIdea = null
  momentPointIdea = null

  // not used; hypothetical technical ideas
  // basicNewIdeaFromOtherRealities = null
  // noEmaginationExistsForThisReality = null
  // emaginationEngine = null
  // emaginationEngineRelatedEnderly = null
}

class TallyIndicatorBoardForAllExistences {
  store = null
  program = null
  creator = null
  dialog = null
}

class TallyIndicatorBoardForAllExistencesAndExistenceTypes {
  type = null
}

export {
  TallyIndicator,
  TallyIndicatorBoardForAllExistences,
  TallyIndicatorBoardForAllExistencesAndExistenceTypes
}

