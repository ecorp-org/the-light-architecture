
// Service Imports

import { Tally } from '../../../../../data-structures/database/tally/item'

// Service Provider Imports



// Constants

// const TALLY_TYPE_TALLY = 'tally'
// const TALLY_TYPE_TALLY_ITEM = 'tally-item'

// Data Structures

class CreateTallyAlgorithmTallySettingsTable {

  // Directory Tree Structure Type

  // '1' is 1 file 'service.js'
  // '2' is 4 files
  //    'precompiled.ts', 'compiled.js', 'compiler.js', 'dialogue.ts'
  // '10' is median dependency tree
  directoryTreeStructureType?: number = 10 // 1 - 10

  // Usecase
  usecaseNameList?: string[] = []
  usecaseDataStructureNameList?: { [usecaseName: string]: [] } = {}

  // Programming Language + Programming Language Environment
  useTypeScriptFiles?: boolean = true

  // Informational Advertisements For Programs
  initializePermissionRuleFiles?: boolean = true
  initializeEventListenerFiles?: boolean = false
}

class CreateTallyAlgorithmUsecaseInput {
  tallySettingsTable: CreateTallyAlgorithmTallySettingsTable = new CreateTallyAlgorithmTallySettingsTable()
  outputLocationAddress: string = ''
  permissionRuleKeyList: string[] = []
}

class CreateTallyAlgorithmProviderOutput {
  tally: Tally
}

// Algorithms

async function createTallyAlgorithm (usecase: CreateTallyAlgorithmUsecaseInput, _miscellaneous: any): Promise<CreateTallyAlgorithmProviderOutput> {

  // Create the usecase list of folders and files

  // Allow the user to start computer program manager for tally
  // to do things like automatic updates to the software??
  // hotreloading????????

  // 
  return new CreateTallyAlgorithmProviderOutput()
}

class CreateTallyAlgorithmService {
  createTallyAlgorithm
}

const _service = {
  defaultServiceClass: CreateTallyAlgorithmService,
  defaultServiceInstance: new CreateTallyAlgorithmService()
}

// Service Export

export {
  _service
}



