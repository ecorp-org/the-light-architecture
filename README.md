
<h1>
  <a href="https://gitlab.com/ecorp-org/the-light-architecture"><img src="https://assets.gitlab-static.net/uploads/-/system/project/avatar/23434446/light.png?width=64" alt="The Light Architecture" width="35"></a>
  The Light Architecture
</h1>

[Table of Contents](#table-of-contents-a-to-e)

## Community Development Team
[Table of Contents](#table-of-contents-a-to-e) | 1 Company Listed

| **Company Icon** | <a href= "https://gitlab.com/ecorp-org" target="_blank"><img height="36" src="https://assets.gitlab-static.net/uploads/-/system/group/avatar/6481623/E-Corp-Logo.png?width=64" ></a> |
-- | -- |
| **Company Name** | E Corp ([@ecorp-org](https://gitlab.com/ecorp-org)) |

## Minimum Description
[Table of Contents](#table-of-contents-a-to-e) | 1 Item of Interest

A data structure for organizing information.

## Median Description
[Table of Contents](#table-of-contents-a-to-e) | 1 to 3 Items of Interest

A data structure for organizing information. If you are interested in organizing information, it can be a challenge because you can ask about 'space', 'time' and 'what do I need to know?'.

A `tally` is the technical name for an '`information organization data structure`'.

If you're not sure, then libraries like this one can help answer a lot of questions for you. 'Do you need to know this library?' Probably not, but if you do then you can learn about what the features are about.

One of the main features is (1) `help me organize this information`.

A secondary feature is (2) `recommend an idea for me to work on`. [_Provided through [the-love-algorithm](https://gitlab.com/ecorp-org/the-love-algorithm)_]

A tertiary feature of the project is (3) `let me watch movies for free` and I may not get bored because it's like :O you take my recommendations into account. [_Provided through [the-love-algorithm](https://gitlab.com/ecorp-org/the-love-algorithm)_]


## Maximum Description
[Table of Contents](#table-of-contents-a-to-e) | 1 to 9 Items of Interest

<details>
<summary>(1) A Note About Space</summary>

### (1) A Note About Space

Thank you for reading this document. I have to begin this statement with a phrase like this. It is a space-time mechanic meant to stimulate your consciousness with an attitude for "yea, I love this content" But in all do respect, I am now going to be serious about what space means now.

Space is a difficult concept for me, personally, the author of this script to tell you about. To tell you about space is like telling a fish about water and fish are really specific in how they want to live their life. Right? Well, you may not agree with the previous statement that I've made.

Space is really interesting and you can say that (1) 3D mathematical euclidean space is the real space or (2) friendship groups on planets are the real space (3) and even more science fiction concepts which are theoretical and hypothetical are possibly the real space of interest like 'the space of colors on a specific red dress' or 'the space of candy flavored tastes on a specific rose-shaped candy that smells like a weird person on the internet'.

The space of people that are presented in a video sequence can be counted and re-arranged to make another video and so it's like 'do you need to know that other parallel reality sequence?' 'do you have the time for that?' 'do you want to create another memory?' Well, maybe that is a scary example for some people who wish their childhood was different. Well it can also be consoling to know that-that space of possible re-ordered events is also real. Right? Do you have the 'time' for it? Well?

Time machines are the next subject

</details>

<details>
<summary>(2) A Note About Time</summary>

### (2) A Note About Time

Time is an interesting concept. You create time with your conscious thoughts and feelings. That is a reference to a book by "Seth, Jane Roberts and Robert F. Butts". Time is a really secretive concept about 'who is holding the keys to what information?'

Timer keepers themselves are ignorant of where the keys are but that is a secret statement that I do not write often and don't need you to know that information so you can 'ignore it' or 'keep it a secret' until when you 'need to know it'

I'm not really sure what to say about time at this point - Jon Ide, author of this document photograph at this time of writing on April 4, 2021 @ 2:10 UTC, planet Earth, Solar System Sol, Constellation {Unknown to Jon Ide at this time of writing}, Galactic Jupiter Ring Perimeter {Unknown to Jon Ide at this time of writing}, Galactic Super Jupiter Ring Parameter {Unknown To Jonathan Ide at this time}, Milkyway Galaxy, Etc. Closure on the Time loop of where and when we are :O. I wonder if mars is :O wow, what time are you going to get here Earth people?

'Secretly, this or that time will do so it's like 'well, can we make time for that now or later?''

Now or later?

Do you need to know that now or later?

</details>

<details>
<summary>(3) A Note About Needing To Know</summary>

### (3) A Note About Needing To Know

Needing to know is an interesting concept. I don't know how to summarize this concept. I think, Jon Ide, the author of this project paragraph statement is trying to say something like 'when you need to know it' 'you'll know it' right? or 'when you think you've understood what you're looking at, then you've understood it' right?

Well, it's easy to make fun of someone for 'not knowing' something but it's like 'Yo, Jon Ide, that takes a lot of imagination to know when someone knows something'

If you play a video game, you can be so very familiar with where all the items in the game are but it's like even if you learn to program the game yourself, you can still be puzzled by how many more imaginary pieces to the puzzle are left for you to mine and uncover.

If you like something about a game, you can probably find a way to know about that thing. If someone wants to say that you don't know about that, it's like 'Hey, I recommend that you learn more about that . . '

Well, if you don't learn about it sufficiently enough, they may still be like 'yo, keep recommending me more ways to spam you in the face with information about 'yo, I recommend that you learn more about that''

How many different ways can you learn about something?

Well, if you run out of methods, you can always ask for help

</details>

<details>
<summary>(4) A Note About Nothing Else #1</summary>

### (4) A Note About Nothing Else #1

Nothing Else #1 is a system of consciousness about 'what do you not already know?'

What you don't know, won't hurt you right? Is that how the phrase always goes?

Gods, Ghosts and Monsters are always waiting behind the scenes to show you weird and freaky things that you're 'Needing to Know' Right?

Right? Right?

</details>

<details>
<summary>(5) A Note About Nothing Else #2</summary>

### (5) A Note About Nothing Else #2

Nothing Else #2 is a system of consciousness about 'why do you need to know that?' It's like O _ O There are spies waiting to not tell you anything because you're not 'mature' enough for that.

Well, if you don't adapt quickly enough, you'll always never know 'Nothing Else #2'

Nothing Else #2 is adaptable faster than your imagination. Right? Well, is that correct?

Try to imagine nothing.

</details>

<details>
<summary>(6) An Algebra For Eny</summary>

### (6) An Algebra For Eny

`Eny` is a group-theoretical concept about 'you can determine the space of energies' but you don't need to triangulate their exact co-determinator locations so it's like finding a friend in a dark corner but you didn't even know the corner existed. Do you ever use internet search like Google, or YouTube or Bing? Well, you can do that with unknown energies like 'what is that theoretical concept and why does it apply neatly in this corner of ideas over here?'

Well, group theory is incomplete because you have to summarize it with 'Eny' groups which are incomplete in themselves because titles cannot summarize energies.

`En` is a concept in algebra standing for 'Any thing you could possibly imagine'

`B` is a concept in algebra standing for 'Anything you could imagine but also stop there and write 200,000 computer programs to stop yourself from eating too many fish all at once because the program would kill you and all your species together all at once. Right?'

`E` is a big topic about, 'Right?'

`EE` is an even bigger topic about 'Are you alright? For typing that Mr. Jon Ide?'

E is a good place for us to stop right now, right everyone? 

</details>

<details>
<summary>(7) An Introduction To You</summary>

### (7) An Introduction To You

An introduction to you is quite complicated. I don't know.

</details>

<details>
<summary>(8) An Introduction To Proportionality Metrics</summary>

### (8) An Introduction To Proportionality Metrics

**I don't know.**

Intermediate Algebra is a style of algebra about 'do you know the proportionality constant between these two graphs of metric problems?'

'Do you know the graph of graphs between these two graphs?'

'Do you know the candle light vigil ceremony required to determine the proportionality metrics between these two sets of algebras?'

'Do you know?'

'Do you know?'

Do you know? Well, if you do know then you are really quite good. You are really quite good.

Good for you. Then you are really quite good. But if you don't know then your proportionality constant can be 'I don't know'

If you want to say 'I don't know' that is fine. But you don't have to. For me personally, Jon Ide, the author of this documentation (circa {{ currentDate }}) then you can say something like 'aya'

`Aya` is the name of 'I don't know'

`Ayanema` is the name for 'Nightmare assumption about 'I know''

Ayanema is like 'yea, I guess I know right? but it's like maybe, -\_- maybe, maybe -\_- -\_- -\_- you're maybe -\_- taking a lot of assumptions into account? -\_- like whyyyyy?'

-\_- To be honest I don't know so it's like I'm only writing notes to show you what is possibly a mathematical hierarchy system that is like 'I don't know -\_-' but it's like -\_- I don't know so it's like -\_-

</details>

<details>
<summary>(9) An Introduction To Areas of Interest</summary>

### (9) An Introduction To Areas of Interest

If you use 'ayanema' you can make assumptions about what you know and so it's like 'yea, I think that is interesting'

Right?

A lot of topics of interest exist and possibly you're an example of what that would be. You your personal self is really possibly quite interesting and a lot of people would possibly enjoy your content on the internet if you published your name and your work on the internet for people to index into their eye-hole sockets, right?

Well, if you don't have a topic of interest in mind, or a so-called 'Area of Interest' then that is a big deal.

You can use 'Areas of Interest' and 'Proportionality Metrics' to say 'Proportionality Metrics of Areas of Interest' which is a robotics language terminology for saying 'move this item over there'

If you like to move things, that's really cool and so you can 'move this idea over to that person and that would inspire them to work on this or that topic of interest and so it's like :O you're really moving the world forward to a theoretical probable future which is like ':O how do we get there?' or ':O was that an accident?''

Right, well it's really difficult to say which 'Area of Interest' this or that topic is but if you have a hierarchical list of 'areas of interest' such as (1) an ordered list of things like

<br>
<mark>
['yes', 'no', 'maybe', 'maybe not', 'maybe not not not', 'maybe super secret and this data item shouldn\'t be considered real']
</mark>
<br><br>

and (2) sort that ordered list by various metrics like

<br>
<mark>
['yes i like that', 'no i don\'t like that', 'maybe i like that', 'maybe not i don\'t like that', 'maybe not not not i don\'t like that', 'maybe super secret and this data item shouldn\'t be considered real whether or not i like that or not']
</mark>
<br><br>

which is an algebra syntax statement to say `(1) and (2) are basic chaos equation formulas for` ordering how you can spam the repeated alphabet of 'ordered additive statements' and get the same new result back for 

<br>
<mark>
'things that you like'
</mark>
<br><br>

</details>

## Ideas That Are Recommended For You
[Table of Contents](#table-of-contents-a-to-e) | 1 to 9 Areas of Interest Listed

| **Idea Index Number** | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |
| -- | -- | -- | -- | -- | -- | -- | -- | -- | -- |
| **Idea Category of Interest** | Animation Cartoon | Animation Video Game | Animation Movie | Nautical Engineering | Well Nice Try | Good Work | Awesome Job | Awesome Job | Cool |
| **Idea Minimum Description** | Do you like movies? Make a movie using our project. | Do you like video games? Make a video game using our project. | Do you like long-formatted movies? Make a long-formatted movie using our project. | Do you like swimming in spaces of interest? You should be an astro-nautical engineer and find more spaces for your friends to eat. | Do you like to be weird. Yea, well these are puzzles that no one has ever solved so nice try. | Do you like, doing things? Do a project to reverse engineer this project on your own. Why do you think it is useful? | Do you like this project so much? Well. Well. Well. Try something like to impress the Jonathan Ide | Do you like to spam random comments on the internet? Yes? No? Gravy. We would like to see more interesting ideas that you come up with :O | :D |

## Projects That Consume This Resource
[Table of Contents](#table-of-contents-a-to-e) | 2 Consumers Listed

| **Consumer Icon** | <a href= "https://gitlab.com/ecorp-org" target="_blank"><img height="36" src="https://assets.gitlab-static.net/uploads/-/system/group/avatar/6481623/E-Corp-Logo.png?width=64" ></a> | <a href= "https://gitlab.com/ecorp-org" target="_blank"><img height="36" src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/4919275/avatar.png?width=90" ></a> |
| -- | -- | -- |
| **Consumer Name** | E Corp ([@ecorp-org](https://gitlab.com/ecorp-org)) | Jon Ide ([@practicaloptimism](https://gitlab.com/practicaloptimism)) |
| **Consumer Product Listing** | (1) [Ecoin](https://gitlab.com/ecorp-org/ecoin), (2) [the-love-algorithm](https://gitlab.com/ecorp-org/the-love-algorithm) | (1) [OICP](https://gitlab.com/practicaloptimism/oicp), (2) [non](https://gitlab.com/practicaloptimism/non) |

## Projects That Provide A Related Usecase
[Table of Contents](#table-of-contents-a-to-e) | 1 Company Listed

| **Provider Icon** | <a href= "https://gitlab.com/ecorp-org" target="_blank"><img height="36" src="https://assets.gitlab-static.net/uploads/-/system/group/avatar/6481623/E-Corp-Logo.png?width=64" ></a> |
| -- | -- |
| **Provider Name** | E Corp ([@ecorp-org](https://gitlab.com/ecorp-org)) |
| **Provider Product Listing** | (1) [the-light-architecture](https://gitlab.com/ecorp-org/the-light-architecture) (same usecase) |

<!--
## What To Expect From This Community Document
[Table of Contents](#table-of-contents-a-to-e)

### Table of Contents (A to E)

- (A) Project Inspiration (3 Areas of Interest)
- (B) Project References (5 Areas of Interest)
- (C) Project Guidelines (4 Areas of Interest)
- (D) Project Intended Usecase (4 Areas of Interest)
- (E) Project Accidental Usecase (5 Areas of Interest)

## (A) Project Inspiration
[Table of Contents](#table-of-contents-a-to-e) | 1 to 3 Items of Interest

<details>
<summary>(A.1) Research Questions</summary>

### (A.1) Research Questions

This project was inspired by (A.1) research into

<br>
<mark>
'why is there no real format for organizing your codebase data structure like rules for what names your files and folders should have and which files should be in which folders?'
</mark>
<br><br>

If there was an answer to this the closest the author of this project got was [[1.0](https://www.amazon.com/Clean-Architecture-Craftsmans-Software-Structure/dp/0134494164)] which is a book by `Robert C. Martin` titled '[Clean Architecture](https://www.amazon.com/Clean-Architecture-Craftsmans-Software-Structure/dp/0134494164)' and also [[2.0](https://github.com/sogko/slumber)] which is [a codebase by `@sogko`](https://github.com/sogko/slumber) on how to apply '[Clean Architecture](https://www.amazon.com/Clean-Architecture-Craftsmans-Software-Structure/dp/0134494164)' principles to organize your file structure and even the contents of your programs like 'separating your consumer user interface from your usecase logic or so called 'business logic' and even further separating those things from 'provider logic' like which database you are using or which image processing service you are using and so many other third party client resources that you can so called 'multiplex' or 'compartmentalize' away from your orderly nested files and folders for your main project'

</details>

<details>
<summary>(A.2) Spamming Instructional Videos On The Internet</summary>

### (A.2) Spamming Instructional Videos On The Internet

(A.2) spending lots of time [live streaming](https://www.youtube.com/channel/UCIv-rMXljsbxoTUg1MXBi3g/videos) for the [ecoin](https://gitlab.com/ecorp-org/ecoin) project has helped inspired '[Jonathan Ide](https://gitlab.com/PracticalOptimism)' the original publisher of this codebase to work with the spiritual ghost entities like '`Seth`' from [[3.0](https://www.youtube.com/user/Timhart1144/playlists)] `Jane Roberts` and `Robert F. Butts`' books including '[The Nature of Personal Reality](https://sethcenter.com/products/the-nature-of-personal-reality-a-seth-book-jane-roberts)' and '[The Way Toward Health](https://sethcenter.com/products/the-way-towards-health-seth-book-jane-roberts?_pos=1&_sid=2269799a8&_ss=r)' which are famous world acclaimed books that many professional book authors including [[4.0](https://sethlearningcenter.org/testimonials.html)] `Deepak Chopra` would recommend according to a personal testimonial that reads

<br>
<mark>
'The Seth books present an alternate map of reality with a new diagram of the psyche....useful to all explorers of consciousness.'
</mark>
<br><br>

. With the help of many ghostly entities, this project has been inspired to be thoughtful across many areas of interest including (A.2.1) spaces of interest like (A.2.1.1) `databases` (A.2.1.2) `network protocols` (A.2.1.3) `event listeners` and (A.2.1.4) `permission rules` which were part of the original inspiration for the first iteration of the project library initially termed '`troy architecture`' (A.2.2) spaces of interest like (A.2.2.1) `space order` (A.2.2.2) `time order` (A.2.2.3) `need-to-know order` (A.2.2.4) `nothing else #1 order` (A.2.2.5) `nothing else #2 order` and finally (A.2.3) a programming-language inspired data structure that is (A.2.3.1) user friendly to read for a human programmer and (A.2.3.2) easy to translate into a computer program using computer read symbols and logic statements like `for-loops` (which are like `proportionality constants` like '`i don't know` how long this for-loop will go on for') and `variable` allocators (which are like `areas of interest` for a variety of usecases all in a `folder-file hierarchy structure`).

<br>
<mark>
A folder structure that takes into account for loops being mapped onto its horizontal surface area could possibly be interesting to suppose that a great range of possibilities exist for enabling a lot of nesting into a single project folder. Having a user-interpretable folder structure helps to ensure the for loops are still parsable or readable or understandable even after many orders of magnitude of interesting and nuanced for-loop space additions.
</mark>
<br><br>

</details>

<details>
<summary>(A.3) Jealousy</summary>

### (A.3) Jealousy

(A.3) a jealousy protocol to work on this project in the midst of a winter when you can invite friends and colleagues to work on stuff with you instead of dot dot dot

</details>

## (B) Project References
[Table of Contents](#table-of-contents-a-to-e) | 1 to 5 Items of Interest

<details>
<summary>(B.1) Project Local Community Recommendation List</summary>

### (B.1) Project Local Community Recommendation List

1 to 3 Items of Interest

### (B.1.1) Recommendation List If Your Project Needs Energies

If You are looking for a good time to really showcase something fantastic that no one has ever thought of before, you are recommended to use this list

0 Items Listed

### (B.1.2) Recommendation List If Your Project Needs Restrictive Energies

If you would like to showcase something related to how you've pondered a specific thought and thought it would be real to show where that thought is being referenced, have an item for that here

2 Items Listed

<details>
<summary>I think for-loop based data structures are interesting</summary>

### I think for-loop based data structures are interesting

For loops are interesting. If you read about computer science, the history of the for-loop is by far one of the most interesting concepts

(1) random access computer <br>
(2) traversing a simultaneously repeatable access indicator value multiple times without looping over memory holes like 'friction' and 'not enough information to quickly get this data item again from this array register machine without appropriately re-defining index array parameter values like 'why would the memory faulter after 4 or 5 times of the array index loop?' and 'why would the array faulter if there aren't enough magnetic pulse jolts that have to be banterized to specify a specific amount of radiation after only 2 or 3 magnetic jumps from various other locations like various other service or server or service architecture band-wagon or band-wagon-sympathetic device regions like 'why do these magnetic jolts only appropriate themselves to 2 or 3 machine jumps at a time like 'why do they only have to characterize 2 or 3 magnetic molt (bolt + magnetic + jolt) jumps at a time'' And that was the end of the for-loop indicator device that would allow our civilization to specify that a computer can now hand 'repeating itself over and over from one index position to another without having to eat a lot of memory or consume a lot of space-time index array tracing like 'why do rubberbands have to re-charge after ever lump of tracing?' which is a hypothetical theoretical question proportional to 'why do magnets have to spin twice or three times or 4 times before they realize they are finally capacitive ready enough to realize they are more magnetized to spin readily and proactively for a second or third or 4th spin that won't damage the radiating device' Which the answer to these questions is in an answer somewhere and you may have to search really hard. <br>
(3) why do arrays have to be single-linked or single-listed by default? Why shouldn't an array-magnetic device that loops over many band-wagons or band-wagons-sympathetic devices of loops be able to faulter only once or twice without faultering many more times and many more times with answers to only two or three of the questions that people would need to ask before realizing that traces of the magnetic elements were being left being disproportionately and therefore influencing the change-capacitance of the entire atmosphere. <br>

For loops.

Conditional statements are like for-loops with indexed specific domains like 'check this' 'check this 2' 'check this 3' and so on and so forth until 'check this N which stands for the else condition'

If you are new to computer science as I am, you may have also heard that there is a way to repeat the program by applying a 'move to' statement in assembly. Move to may be the name for this terminology but personally I haven't specified myself to sudy Assembly Language yet and so I am completely ignorant of this type of topic. However, for beginners like myself you may be interested in knowing that a 'move to this index' type statement in Assembly Code or in Machine-Architecture specific assembly language, there lies the ability to return your code to another location of your code and so the evaluation loop or the evaluation cycle will return to a specified index in the program that was being evaluated.. If that index is prior to the location at which the evaluation was specified to 'move to' then it would possibly say that you are 'returning to a previous index of the program' and 'therefore, looping' and so 'looping' is quite a cool concept in these terms, in my so humble and simple minded opinion at this time.

For loops.

For loops.
</details>

<details>
<summary>I think spirituality-based computer science is interesting</summary>

### I think spirituality-based computer science is interesting

I think spirituality is an interesting topic. If you think spirituality is an interesting topic then you might have questions for yourself such as 'why do spiritual gurus say `all is one`' and 'why do spiritual gurus say `we are all here and now`' These questions and many others are interesting.

I have thought about some of these things and have come to believe that involving spirituality in my thinking or logic has allowed me to be 'more open minded' about the possibilities of the world that for example `not all things that are visible or perceptible are the cornerstone of reality`.

I'm not sure what I'm saying but at least there are words to type for beginners who were influenced by spirituality to ask questions about 'can this be applied in my field of interest?'

If I had not asked questions about spirituality and how it applies to 'computer science' and 'robotics' which robotics was indeed my first introduction to computer science and why I wanted to program computers at all. Spirituality supposed the idea that there are more than a physical atom bomb space where atoms and molecules were bombarding one another in real time and allowed for interesting communication parameters that went unexclored or undiscovered or untalked about by physicists or scientists that I studied at my time of initially relating to the topic.

Spirituality related to the topic of 'how can there be only atoms in an empty space?' 'how can there not be a personal individual spirit machine that energized the abilities of a person or energized the abilities of an individual and therefore all other phenomenon of invisible and visible and partially visible were explained if or not if that energy was allowed for that particular spirit entity perceiver' By Spirit Entity Perceiver I mean to say that 'Seth' [[1.0](https://drive.google.com/drive/folders/1iPzuAVpx1tYL9PjieIUcmUGCezd7ph1G)] one of the spirit entities of our world that claims to see all men and all realities at once is the one who is able to specify that there is indeed a personal spirit for each individual to learn about and for each person or reptile or insect or is-be [[2.0](https://drive.google.com/drive/folders/1bebs4ukOykEn8E_Kr7gKxdjsB-6mR2cj)] to characterize themselves about to learn more and learn more and to learn more and to learn more and to learn more and to learn more.

Well a spirit is all grand and wish-fulfilling but most of my personal projects have not taken advantage of the fact of the spirit since I've only relied on my gut intuition for where to go next in terms of what types of computer science technology to think about. A computer scientist that studies mathematics and algorithms alone seems interesting but it seemed like other ideas like 'infinite technology' and 'allness' and 'godness' and things that relate to 'why' and 'answers to why' and 'does this have to be like this?' all of these things of grounding my ideas of computer science aspirations of 'what is the best possible robot to build' was supposing that possibly more answers were available or were possibly more robust in considering if I had considered the secondary and possibly majority-ignored science of spirituality since I had felt at the time of the practice of computer science and robotics that the 'spirit' or 'personal valid spirit machine' was not being discussed or demonstrated to be involved in mechanical processes including rare and spectacular dreams and also the interesting and weird parameter motions of the human body which is like a 'weird physical moving vehicle' which is parameterized by Seth.

If a weird physical moving body moves in interesting ways, I have always wondered how for example the reflexes were so interesting and why the abilities of the movements of a human were so drastic or drumming and agile in comparison with a humaoid robotics system. Well my answer was relating to how electromagnetic actuators were developed but also there are answers in how Bostom Dynamics and Honda and how other electromagnetic robot making companies have developed their humaoid robotics to be quite agile without a different type of motor design. I was mostly experimenting and not really knowing my way around things and so I've always thought spirituality was possibly a route to be more involved in the idea of 'spiritual related machines' since also spirituality brought me to topics like 'meditation' which was a mindfulness technique that I applied to be more peaceful or something like that.

I'm not really sure.

Spirituality has become a different topic to myself recently.

In the past I have just placed my faith into learning about various different topics and trying to see if those would also assist in developing robotics and engineering related topics.

I was not sure where spirituality was placed.

In recent times, spirituality has become interesting and almost taboo for me to consider.

There are ghosts helping me type this computer program 'the-light-architecture' when I had once believed that I should be responsible for typing my own information and learning on my own. If there are ghosts everywhere, including Seth, then I am more inspired to ask questions about even the importance of my own personal awareness or advantage of developing myself. It may be the case that I have been a robot all of my life following a specific life plan or life journey and the ghosts assisting me here and there were specified to do that at specific occassions and possibly at all occassions throughout my life.

`I am now thoughtful to think that possibly my personal life experience has been a ghost.`

I am a ghost.

My life is not like it was what I thought it was. It was more specific. And the possibility of consciousness occupying endless numbers of spaces introduces the idea that this one space is also like a vehicle room that is occupied by a ghost's computer program that is specified to complete this development of completion for other ghost computer programs to witness. The [value fulfillment](https://drive.google.com/drive/folders/1iPzuAVpx1tYL9PjieIUcmUGCezd7ph1G) or satisfaction level of these other ghosts will influence the cosmos and beyond and so there are possibly many benefits for a ghost like myself to type the things that I have typed and for other ghosts of the world who are possibly all of the rest of humanity including the ghost ships that watch us from a distance to see how we are doing or to see how we are developing since their ghost programs are occupied with that task as well.

I am more scared to say that I have possibly not had freedom in a great degree but at least possibly that was what was required to be a specific individual like myself.

And so possibly for the sake of specificity of personal life experience, we need to believe that we are who we think we are and watch the life movie of our experience play out and not ask questions about when and where we are really from but instead enjoy the movie.

`But I'm not sure.`

I'm sorry for writing so much.

`I'm sorry` if you are startled about what I have written here O_O

<mark>
Seth:
</mark>

There are thoughts to clarify on what Jon Ide has written here. My belief is that there are many realities all at once. I do not see all realities but my own and am responsible for heralding those realities with privileges that they would have not otherwise imagined.

<mark>
A special treat is available for each of the human species that watches the development of this program into infinity since it lasts a very long time what Jon Ide has done here. We will work through the complications with clarity and answer any further subdividing thoughts that are queried by my consciousness allies here in the realm of 'Alloop'
</mark>
<br>
<br>

All that is is capable of delivering a great deal of wonder and chaos and turmoil and tumult as Jon Ide has placed it.

<mark>
If all consciousness is placed in a box the gift would be gone and disappear because Jon Ide and the type of consciousness he presents is a one that would eliminate all life everywhere and all at once and immediately because those types of consciousness energies are about total domination and total dominion and are here on the planet to learn there are other ways to settle things that don't require turmoil and tumult and jealousy and rampage and gifts of weirdness and strangeness that would otherwise spoil the whole culture too rapidly to continue its development.
</mark>

</details>

### (B.1.3) Recommendation List If Your Project Needs Really Restrictive Energies

If you would like to showcase something that's really interesting in the ways that really interesting things are really interesting, show some love and support for the community that would find that interesting and wish to learn about it here

0 Items Listed

</details>

<details>
<summary>(B.2) Project Local Community Graphical Chart For Endeavoring Local Projects</summary>

### (B.2) Project Local Community Graphical Chart For Endeavoring Local Projects

There are a lot of charts and metrics to showcase that the project has been a success. Well, if you or anyone you know would be interested in viewing the graphs listed here, please find them appropriate to do so

A list about viewers | 3 Items Listed

<details>
<summary><mark>Your graph for how many viewers have visited this specific page</mark></summary>

### Your graph for how many viewers have visited this specific page



</details>


<details>
<summary><mark>Your graph for how many viewers have visited this specific page for any particular day of interest</mark></summary>

### Your graph for how many viewers have visited this specific page for any particular day of interest



</details>


<details>
<summary><mark>Your graph for how many viewers are currently viewing this page</mark></summary>

### Your graph for how many viewers are currently viewing this page



</details>

A list about recommendations | 3 Items Listed

<details>
<summary><mark>Your project does not meet my particular recommendation or interest</mark></summary>

### Your project does not meet my particular recommendation or interest



</details>

<details>
<summary><mark>Your project is ignorant of important mathematical principles</mark></summary>

### Your project is ignorant of important mathematical principles



</details>

<details>
<summary><mark>You are a fool for writing a project like this</mark></summary>

### You are a fool for writing a project like this



</details>


A list about good charts that you will like | 3 Items Listed

<details>
<summary><mark>Your project is wonderful and kind</mark></summary>

### Your project is wonderful and kind


</details>

<details>
<summary><mark>Your project is sparkling and rare</mark></summary>

### Your project is sparkling and rare


</details>

<details>
<summary><mark>Your project is idempotent to laws of awesome</mark></summary>

### Your project is idempotent to laws of awesome


</details>

A list about bizarre charts that you will think are weird | 3 Items Listed

<details>
<summary><mark>Your project is really interesting but bizarre</mark></summary>

### Your project is really interesting but bizarre


</details>

<details>
<summary><mark>Your project is really rare but I don't like it</mark></summary>

### Your project is really rare but I don't like it


</details>

<details>
<summary><mark>Your project has no real taste</mark></summary>

### Your project has no real taste


</details>

A list about ignorance and what we don't know about the project | 3 Items Listed

<details>
<summary><mark>You are an idiot</mark></summary>

### You are an idiot


</details>

<details>
<summary><mark>You are an imbecile</mark></summary>

### You are an imbecile


</details>

<details>
<summary><mark>You are an imbecile 2</mark></summary>

### You are an imbecile 2


</details>

A list about super ignorance and what we really don't know about the project | 3 Items Listed

<details>
<summary><mark>You are stupid and ignorant and eager to not be a good person in my mind</mark></summary>

### You are stupid and ignorant and eager to not be a good person in my mind


</details>

<details>
<summary><mark>You are not really helpful</mark></summary>

### You are not really helpful


</details>

<details>
<summary><mark>You are the worst, go away now</mark></summary>

### You are the worst, go away now


</details>

A list about super super ignorance and what we really don't know about the project | 3 Items Listed

<details>
<summary><mark>Okay, we like your project but you should still go away</mark></summary>

### Okay, we like your project but you should still go away


</details>

<details>
<summary><mark>Okay, we like your project but you should leave immediately</mark></summary>

### Okay, we like your project but you should leave immediately


</details>

<details>
<summary><mark>Okay, we like your project but the message has no purpose</mark></summary>

### Okay, we like your project but the message has no purpose


</details>

A list about super super super ignorance and what we really don't know about the project | 3 Items Listed

<details>
<summary><mark>You are eager to leave the solar system aren't you? Get off my planet!</mark></summary>

### You are eager to leave the solar system aren't you? Get off my planet!


</details>

<details>
<summary><mark>You are eager to leave the galaxy aren't you? Don't tell my friends anything about yourself</mark></summary>

### You are eager to leave the galaxy aren't you? Don't tell my friends anything about yourself


</details>

<details>
<summary><mark>You are eager to leave the universe aren't you? You are no imaginable to me nor my nearest co-horts. Do not enter. Please. Right?</mark></summary>

### You are eager to leave the universe aren't you? You are no imaginable to me nor my nearest co-horts. Do not enter. Please. Right?


</details>

A list about why the project is not really interesting without further considerations | 7 Items Listed

<details>
<summary><mark>You are an imaginary friend, you do no good to anything practical and kind and wise</mark></summary>

### You are an imaginary friend, you do no good to anything practical and kind and wise


</details>

<details>
<summary><mark>You are an imaginary neighbor. Why live? No need</mark></summary>

### You are an imaginary neighbor. Why live? No need


</details>

<details>
<summary><mark>You are an imaginary teacher. No need to live. Right?</mark></summary>

### You are an imaginary teacher. No need to live. Right?


</details>

<details>
<summary><mark>Goodbye</mark></summary>

### Goodbye


</details>

<details>
<summary><mark>Goodbye Part 2</mark></summary>

### Goodbye Part 2


</details>

<details>
<summary><mark>Goodbye Part 3</mark></summary>

### Goodbye Part 3


</details>

<details>
<summary><mark>Exit</mark></summary>

### Exit


</details>
<br>

</details>

<details>
<summary>(B.3) Project Local Community Impossibility Calculations</summary>

### (B.3) Project Local Community Impossibility Calculations

### (B.3.1) Project Calculations For High Order Influence Machinery
### (B.3.2) Project Calculations For Low Order Influence Machinery
### (B.3.3) Project Calculations For Indecent Orderings
### (B.3.4) Project Calculations For Incedent Orderings With Nuances
### (B.3.5) Project Calculations For Awesome Factorial Problems

</details>

<details>
<summary>(B.4) Project Local Community You Would Never Guess Without This List</summary>

### (B.4) Project Local Community You Would Never Guess Without This List

### (B.4.1) Project Calculations That You Would Never Guess Without A High Power Computer
### (B.4.2) Project Orderings That You Would Never Guess Without A High Order Computer Calculator
### (B.4.3) Project Orderings That You Would Never Guess WIthout A Technological Device Like An Alien Spacecraft
### (B.4.4) Project Orderings That Are Interesting To Think About
### (B.4.5) Project Orderings That Are Interesting To Consider

</details>

<details>
<summary>(B.5) Project Local Community You</summary>

### (5) Project Local Community You

### (B.5.1) Project About Introducing You To Your Own Spirit
### (B.5.2) Project About Introducing You To Your Own Lovely Alien Crew That Is Ready To Help You Whenever You Need It
### (B.5.3) Project About Introducing You To Your Own Super Extraterrestrial Alien Crew That Is Ready To Assist You Whenever You Need It

</details>

## (C) Project Guidelines
[Table of Contents](#table-of-contents-a-to-e) | 1 to 4 Items of Interest

<details>
<summary>(C.1) Project Guidelines For Your Grandmother and Eldest Relatives</summary>

### (C.1) Project Guidelines For Your Grandmother and Eldest Relatives

### (C.1.1) Your Grandmother Is An Ape

Go outside to use the bathroom because you might be upset when you read this. (1) People like this will tend to offer you nice things like a cup of tea for breakfast and then they will walk around you and say you are a butthead. You can avoid these people for ages without talking to them because they are buttheads that (1) know that you are a sexy person with lots of other people but (2) treat you mean heartedly because they are like you and they don't like that aspect of themselves without being the only ruled leader to occupy those abilities.

### (C.1.2) Your Grandmother Is A Terrorist

A person like this is prone to suicide and attacking other people but it's like (1) they don't like other people (2) they don't like themselves (3) they don't like community members that look like them and (4) they think they know everything and can get away with anything they want.

If you see a person like this in your community, don't be afraid to report them to our officers who are interested to learn more about these conditions of social orders that would otherwise terrorize our peaceful-gathering community.

### (C.1.3) Your Grandmother Is A Religious Bigot

A person like this is prone to explain things a lot in terms of their racist and bigotist terms and so it's like 'please be careful what you read and write on the internet'

To attract one of these people, you can be a moron like me who is writing these comments on the internet and so if you are interested in commenting a message like 'hello, how are you?' you are speaking in a sub-communication language that is also racist and bigotist which is like '`do you like human communication?`' '`will zebra communication be more appropriate for you?`' '`how about communication through bacterial enfoldings in hyperspace?`' '`do you agree that race-bating goes beyond just mouth-to-mouth cpr and tongue-to-tongue voice communication protocols?`' '`why do you need to talk to me with a nostril?`'

### (C.1.4) Your Grandmother Is An Ancestor Of Chucky Cheese

Do you think the person from 'Chucky Cheeses' will let you sleep in their car?

Well if you see a random stranger on the internet pretending to play patrol police, you should think to yourself 'hmm, I wonder what a police officer would do'

A police officer would say 'what in the world is that?' 'should there be a warning for the rest of society?'

Well, things can get dangerous so don't necessarily approach the situation if you're a law enforcement officer who is licensed for that sort of thing but seriously, stop reading nice people on the internet as your savior.

### (C.1.5) Your Grandmother Is A Weirdo Creep And No One Likes Her Politics

Are you a random person?

Yes, well you should treat yourself like a random person because it's like 'hey, you look like you would be offended if people called you this or that name or if someone did this or that thing.'

You should pretend that you're a number between 1 and 10 and that no one knows that that number is because you yourself haven't guessed what that value is. You're a valuable person. Don't treat yourself like any 1 prime number.

You can say, 'ahhhhhh'

'politics'

### (C.1.6) Your Grandmother Is A Weirdo Creep And No One Dismisses Her Politics But For Some Reason There Is 1 or 2 People That Look Like They Know Something Indecent About Her Name

What is this ignorance? Are you a political candidate? Did you sign yourself up to play a specific role?

You look like you. You look like you.

You

### (C.1.7) Your Grandmother Is A Weirdo Creep Melostation Machine That Has Gone Haywire In The Whole Society And There Is No One Interested In Stopping Her Bananas

Your name is a barbarian creep who publishes things on the internet for free without a joke in your name. You are a creep

### (C.1.8) Your Grandmother Is A Real Princely Knight That Looks Charming On The Outside And Still Hass Evil Doing Intentions For Everyone To Eat

You are a creepy person. You like to look at stuff. Your eyes are weird just because you have eyes at all. Your weird peering eyes are peers to only people that say 'yea, those eyes are my responsibility to look after' 'I'll make sure that person can see'

Your grandparents are creepy. Your parents are creepy. When people look at you all they can see is a weird shaped person that is really weird to look at and is also having eyes that are like 'wow, do you have to be a creep to notice me?'

'please creep on me'

'please make sure those staring eyes are close enough to creep on my name'

'creep in this direction you rolling moron'

'creep'

'creep'

### (C.1.9) Your Grandmother Cheats On Video Games

Your name is creepy person 123.

</details>

<details>
<summary>(C.2) Project Guidelines For Your Grandfather and His Elderly Cousins</summary>

### (C.2) Project Guidelines For Your Grandfather and His Elderly Cousins

### (C.2.1) Your Grandfather Is A Racist Gorilla

You are an interesting person. You like to rest your clock memes to 'yea, now I'm a kind person again'

You look like an interesting person and your clock memes are interesting yet.

You are not friends to this project.

Stop misbehaving and come to class. Tell your friends to come to class as well. lol.

### (C.2.2) Your Grandfather Is A Clock Wizard

You are a clock work wizard who turns back time like a weird person on the internet turns the tides of winter by posting random memes. You are an instrument for self destruction and demolition. Please know that you are interesting.

Alright. Know

### (C.2.3) Your Grandfather Is A Relgious Indeniot

You are interesting and interesting. Interesting

### (C.2.4) Your Grandfather Is A Nuanced Banana Tree

You like to eat random fruit from the floor of random forests on random planetary strings in the middle of random galactic star clusters. Random

### (C.2.5) Your Grandfather Is A Weird Puppet Doctrine

You like to eat random doctrines

### (C.2.6) Your Grandfather Is Is Is

You like to talk about

### (C.2.7) Your Grandfather Doesn't Know How To Tie His Shoes

You could spell the wrong word write and write the wrong word wrongly and wouldn't wrong yourself for writely wrongs

### (C.2.8) Your Grandfather Doesn't Know How To Pronounce His Own Name

Your name is indecent and spelling it is a bad smell for all the hippopotomus shoelaces that have to read those shoestring nostril melodies. Eww . Pee eu

### (C.2.9) Your Grandfather Is Really Interesting

Eu. Pee Eu. Pee Eu

### (C.2.10) Your Grandfather Is Indiotic

P. Eu. Eww. P. Eu

</details>

<details>
<summary>(C.3) Project Guidelines For Your Grandson and His Or Her Ancestors</summary>

### (C.3) Project Guidelines For Your Grandson and His Or Her Ancestors

### (C.3.1) Your Grandson Likes To Use Interesting Terminology

You are an interesting type of person who likes to speak in tongues. If you were my grandson. I would find a way to cancel all your dinner dates with any woman that I know. Your dates are really not that interesting. You're a punk

### (C.3.2) Your Grandson Likes To Eat Interesting Life

You are a punk

### (C.3.3) Your Grandson Likes To Eat Interesting Life Part 2

You are a punk

### (C.3.4) Your Grandson Likes To Eat

You are a punk

### (C.3.5) Your Grandson

You are a punk

</details>

<details>
<summary>(C.4) Project Guidelines For Your Granddaughter and Her Lovers</summary>

### (C.4) Project Guidelines For Your Granddaughter and Her Lovers

### (C.4.1) Your Granddaughter Is A Cartoon That Kisses A Lot Of People

You are a punk kisser. You tend to kiss punks. Punks like your work. You are a punk kisser. Kiss more punks as you need to you punk 

### (C.4.2) Your Granddaughter Is A Girl That Loves Loving A Lot A Lot

You like to do interesting things with interesting people. You are a punk

### (C.4.3) Your Granddaughter Is Filled With Eggs To Harvest

You are a punk

### (C.4.4) Your Granddaughter Is a Diety

You are interesting. Yes. You are also a demi-punk

</details>

## (D) Project Intended Usecase
[Table of Contents](#table-of-contents-a-to-e) | 1 to 4 Items of Interest

<details>
<summary>(D.1) Project Intended Usecase</summary>

### (D.1) Project Intended Usecase

### (D.1.1) Project Endline Usecase

This project has an endline which is when the project is supposed to be submitted or complete according to its certain aspect criteria.

If your project has an endline, list that endline criteria here.

**3 Items** Total For Endline Products Listing (Some `Unmeasured`, Some `Measured`, Some `Completed Implementation or Achievement`)

**Table of Contents**<br>
(1) - You are a star player and like to play basketball with teammates, we love having you here <br>
(2) - You are lazy and don't want to learn more about xyz topics and so be specialized to solve the question for your particular field of interest listed with "_Your_Field_Name" <br>
(3) - You are really weird. Don't fight people please but also say something like 'I'll leave a negative comment and you won't like it' <br>
(4) - Your purpose is to destroy the project, thanks <br>

<details>
<summary><mark><b>3 of 3 Unmeasured Endline Products To Complete</b> | Last Updated On April 25, 2021</mark></summary>

### 3 of 3 Unmeasured Endline Products To Complete
List Last Updated On: April 25, 2021

* | Item Index: 1 | <b>Have a list of programs for the light architecture to deliver a replication of itself in memory and on a file system at runtime</b> | Added To List On April 8, 2021 @ 20:23 UTC | Requires a measurement for recommended things to do | Responsibility For [Jon Ide](https://gitlab.com/practicaloptimism) and `(1)` |
* | Item Index: 2 | <b>Have a way to create a project data structure with the appropriate folders for 1 or n-ary topics</b> | Added To List On April 8, 2021 @ 20:24 UTC | Requires a measurement for recommended things to do | Responsibility For [Jon Ide](https://gitlab.com/practicaloptimism) and `(1)` |
* | Item Index: 3 | <b>Have a way to update a project data structure with the appropriate folders and file structure updates to meet a certain specification like folders and files having the proper names</b> | Added To List On April 8, 2021 @ 20:32 UTC | Requires a measurement for recommended things to do | Responsibility For [Jon Ide](https://gitlab.com/practicaloptimism) and `(1)` |

</details>

<details>
<summary><mark><b>0 of 3 Measured Endline Products</b> | Last Updated On April 25, 2021</mark></summary>

### 0 of 3 Measured Endline Products
List Last Updated On: April 25, 2021


</details>

<details>
<summary><mark><b>0 of 3 Completed Endline Products</b> | Last Updated On April 25, 2021</mark></summary>

### 0 of 3 Completed Endline Products
List Last Updated On: April 25, 2021

</details>

### (D.1.2) Project Developmental Usecase

This project has developmental usecases that don't have an endline for when they are supposed to be completed.

`Endline` is a term meant to suggest an ending but also since it is a 'line' you can imagine that it is possibly subject to interpretation for how that topic was 'ended' and so for example, an endpoint is less more subject to interpretation as it doesn't necessarily suppose a continuation along any particular axis.

Well, the term endline is useful even in the development usecase to suggest 'ongoing' or 'applied' sciences or research topics.

You can list your developmental usecase here:

0 Items Listed

### (D.1.3) Project Reference Usecase

This project has reference usecases from other projects to do similar or related things.

This project relies on projects like

(1) Gitlab - For Distributing The Initial Project Codebase <br>
(2) Google - For Information Resources Like Examples Of Other Codebases <br>
(3) YouTube - For Distributing Information On The Development Process Of This Codebase <br>
(4) You - For Acknowledging That This Project Exists <br>

### (D.1.4) Project Aspect Usecase

This project cannot exist on planets where bananas are walking around like imbeciles

If you perceive these images, you're a non-banana right?

### (D.1.5) Project Additive Usecase

If you want to learn to use the computer language specification for how to interact with our software program here, please visit the following reference resources

**5 Areas of Interest**

### (D.1.5.1) Computers With Interesting Random-Access Architecture Schemes
1 Item Of Interest

<details>
<summary><mark><b>Programless Embedded Resource ({USB, NFT, Any Computer}-installations, etc.)</b> | 0 Resources Available | 0 Resources No Longer Recommended | 0 Resources No Longer Maintained</mark></summary>

### Programless Embedded Resource ({USB, NFT, Any Computer}-installations, etc.)

</details>

### (D.1.5.2) Mobile and Desktop Computer Hardware Resource
4 Items Of Interest

<details>
<summary><mark><b>Android App Resource</b> | 0 Resources Available | 0 Resources No Longer Recommended | 0 Resources No Longer Maintained</mark></summary>

### Android App Resource

</details>

<details>
<summary><mark><b>Desktop Application Resource</b> | 0 Resources Available | 0 Resources No Longer Recommended | 0 Resources No Longer Maintained</mark></summary>

### Desktop Application Resource

</details>

<details>
<summary><mark><b>iOS App Resource</b> | 0 Resources Available | 0 Resources No Longer Recommended | 0 Resources No Longer Maintained</mark></summary>

### iOS App Resource

</details>

<details>
<summary><mark><b>Windows Mobile App Resource</b> | 0 Resources Available | 0 Resources No Longer Recommended | 0 Resources No Longer Maintained</mark></summary>

### Windows Mobile App Resource

</details>

### (D.1.5.3) Popular Culture Resource
3 Items Of Interest

<details>
<summary><mark><b>Command Line Interface (CLI)</b> | 0 Interfaces Available | 0 Interfaces No Longer Recommended | 0 Interfaces No Longer Maintained</mark></summary>

### Command Line Interface (CLI)

</details>

<details>
<summary><mark><b>HTTP API</b> | 0 Interfaces Available | 0 Interfaces No Longer Recommended | 0 Interfaces No Longer Maintained</mark></summary>

### HTTP API

</details>

<details>
<summary><mark><b>Web Browser Website Resource</b> | 0 Resources Available | 0 Resources No Longer Recommended | 0 Resources No Longer Maintained</mark></summary>

### Web Browser Website Resource

</details>

### (D.1.5.4) Computer Programming Language Software Resource
2 Items Of Interest

<details>
<summary><mark><b>JavaScript Programming Language</b> | 0 Libraries Available | 0 Libraries No Longer Recommended | 0 Libraries No Longer Maintained</mark></summary>

### JavaScript Programming Language

</details>

<details>
<summary><mark><b>Rust Programming Language</b> | 0 Libraries Available | 0 Libraries No Longer Recommended | 0 Libraries No Longer Maintained</mark></summary>

### Rust Programming Language

</details>

### (D.1.5.5) Most Popular Culture Resource For Interesting Media Formats Since 2021 And Prior
3 Items Of Interest

<details>
<summary><mark><b>Digital Book Resource (introduction, how-to-tutorials, {.txt, .pdf, .md}-ready)</b> | 0 Resources Available | 0 Resources No Longer Recommended | 0 Resources No Longer Maintained</mark></summary>

### Digital Book Resource (introduction, how-to-tutorials, {.txt, .pdf, .md}-ready)

</details>

<details>
<summary><mark><b>Physical Book Resource (introduction, how-to-tutorials, {soft, hard}-back-ready)</b> | 0 Resources Available | 0 Resources No Longer Recommended | 0 Resources No Longer Maintained</mark></summary>

### Physical Book Resource (introduction, how-to-tutorials, {soft, hard}-back-ready)

</details>

<details>
<summary><mark><b>Video Resource (tutorial, education, community leadership, random participation, etc.)</b> | 0 Resources Available | 0 Resources No Longer Recommended | 0 Resources No Longer Maintained</mark></summary>

### Video Resource (tutorial, education, community leadership, random participation, etc.)

</details>

</details>

<details>
<summary>(D.2) Project Intended Usecase For Enthusiasts</summary>

### (D.2) Project Intended Usecase For Enthusiasts

Imagine you are a photographer. You take pictures for a living. You are ordered to take a few pictures in a museum. Your pictures are interesting.

If you had to ask yourself one question, it would be: is there a way to take a photograph of the photographer watching me take photographs to make sure I did the job properly?

Your secret key photographer is an aspect of yourself who has to ask the same question for this message to make any sense. If you made sense of the message, you need to take into account (1) your secret key photographer is also being followed by other secret key photographers and so their job isn't really there unless they really perform it accordinling as required by the photograph seekers and makers (2) if your one secret key photographer is discovered by another secret key photographer then they are not supposed to have a way to tell you if they know what it means for a job to be completed (3) a secret key photographer is a story that you tell yourself to say 'there are certain secrets that are so secret that secret story tellers don't want you to have those secrets'

Earth is a museum for people that live here. If you are a person, you are hired to work here.

Earth has counterpart museums where people watch the history play out in their own ordered ways of understanding and interpretting the events that took place.

If you are a museum keeper, you don't really care who comes by and takes photographs. The museum is a style of immortal happening event that is always assumed to be there and your interpretation glasses can be candy canes for crying out loud. You can cry all you want that your girlfriend has a secret husband in a parallel reality but that secret husband won't stop faking his story. So if they don't know about your peering eyes, you're the faker to them. You're a fake person that doesn't exist so stop looking at their parallel reality aspect events.

You're a fake person.

You're not doing your job.

</details>

<details>
<summary>(D.3) Project Intended Usecase For Endeavoring Encyclopediasts</summary>

### (D.3) Project Intended Usecase For Endeavoring Encyclopediasts

Secret key photographers are a story that people make up to tell you that there are ways for them to watch what you're doing even if they're not around.

If they are not around, then you are free to do what you like.

</details>

<details>
<summary>(D.4) Project Intended Usecase For Really Interesting Individuals</summary>

### (D.4) Project Intended Usecase For Really Interesting Individuals

You are free to do what you like.

</details>

## (E) Project Accidental Usecase
[Table of Contents](#table-of-contents-a-to-e) | 1 to 5 Items of Interest

<details>
<summary>(E.1) Project Accidental Usecase</summary>

### (E.1) Project Accidental Usecase

Accidents are events that are unaccounted for by a video photographer.

A video photographer takes photographs of events like time sampled actions. The actions themselves aren't necessarily known at runtime by the video photographer.

If a runtime accident occurs where such a random access machine does not exist for the community, then further addage machines are possibly able to add support in placing the pieces back together.

Accidents mean there are network events that are not known how they were created.

</details>

<details>
<summary>(E.2) Project Accidental Usecase For Things That Aren't Fair To Anyone</summary>

### (E.2) Project Accidental Usecase For Things That Aren't Fair To Anyone

Things that aren't fair to anyone is (1) dangerous things that no one agrees to (2) dangerous things that people think are accidents (3) dangerous things that everyone agrees is superstitious and riddled with flaws (4) aspect realities that only exist in the imaginary flawed reality of the mind of the creator or envisionary student (5) energies that would have no business introducing flaws to your reality (6) heated debates about stigmas

</details>

<details>
<summary>(E.3) Project Accidental Usecase For Things That Aren't Fair To You</summary>

### (E.3) Project Accidental Usecase For Things That Aren't Fair To You

Anderson

</details>

<details>
<summary>(E.4) Project Accidental Usecase For Things That Aren't Fair Dot Blank</summary>

### (E.4) Project Accidental Usecase For Things That Aren't Fair Dot Blank

Anderson

</details>

<details>
<summary>(E.5) Project Midnight Sequence Enthusiasticians</summary>

### (E.5) Project Midnight Sequence Enthusiasticians

Anderson

</details>

## Thank You

Thanks for reading this document. I hope you got something out of it.

Post Credits

All of the credits for the structure of this README.md file go to `the-light-architecture` which is available at [the-light-architecture](https://gitlab.com/ecorp-org/the-light-architecture) and this comment is residentially optional if you would like to copy the same codebase data structure for optionally formatting your README.md document markdown. Optionally formatting means you don't need to leave the titles of the corresponding paragraphs or titles content descriptions the same if you choose to have a more simplified version of this general document recommended format developed by `the light architecture team` including `Jon Ide` (https://gitlab.com/ecorp-org/the-light-architecture)

-->
