
# Copy-Paste Instructions For README.md

This document is accompanied by the respective data structure of interest:

[README.md Data Structure](./data-structure.md)

# Instructions

`(1)` Search for text

`(2)` Replace all text with the recommended idea

`Notes`: Keep in mind the replacement are examples and you can possibly use variants of the examples provided. For example, github and gitlab may not be the only location ids you may be interested in applying.

# Useful Keyboard Commands

'`Command`' + '`F`' to '`Find a name`' in 🍎 Mac Computer <br>
'`Control`' + '`F`' to '`Find a name`' in 🖼️ Windows Computer <br>

'`Command`' + '`A`' to '`Select All`' for the text on 🍎 Mac Computer <br>
'`Control`' + '`A`' to '`Select All`' for the text on 🖼️ Windows Computer <br>

Select 'Replace All' For that particular name or text element string

# Recommended Idea

### (1) Project Name

Replace with your 'project name' idea

### (2) Project Author Name

Replace with your 'project author name' idea

### (3) project-icon-id

Replace With:
- HTML:
```html
<img height="36" src="" alt="Project Name">
```
- Emoji: ✨

### (4) project-author-icon-id

Replace With:
- HTML:
```html
<img height="36" src="" alt="Project Author Name">
```
- Emoji: 👩

### (4) project-id

Replace with your 'project-id' from 'gitlab' or 'github' or a 'file-name' without 'spaces' or 'weird' 'characters'

### (5) project-author-id

Replace with your 'project-author-id' from 'gitlab' or 'github' or a 'birth-id' or 'unique-identifier'

### (6) project-directory-location-id

Replace With:
- Gitlab: https://gitlab.com
- Github: https://github.com
